package com.automation.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ConnectDB extends TestBase {
    public static Connection connection;

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    public static void connectionToSQL() throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/main/resources/database.properties", "username");
        String password = getPropertyFile("src/main/resources/database.properties", "password");
        connection = DriverManager.getConnection(url, username, password);
    }

    public static ResultSet selectQuery(String query) throws IOException, SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }

/*    public static int updateQuery(String query) throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/classicmodels";
        String username = getPropertyFile("src/main/resources/database.properties", "username");
        String password = getPropertyFile("src/main/resources/database.properties", "password");
        connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        int resultSet = statement.executeUpdate(query);
        return resultSet;
    }*/

}
package com.automation.pages;

import com.automation.core.ConnectDB;
import com.automation.core.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AmazonHomePage extends ConnectDB {

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy(id = "nav-logo-sprites")
    private WebElement amazonHomePage;

    @FindBy(xpath = "//div[@class='a-section a-spacing-small a-spacing-top-small']")
    private WebElement resultNumbers;

    @FindBy(xpath = "//span[@class='a-color-state a-text-bold']")
    private WebElement pageHeader;

    public void validateUserCanClickOnSearchBox() {
        searchBox.click();
        ExtentTestManager.log("Search box has been clicked");
    }

    public void validateUserCanEnterItemNamesAndSearch() throws SQLException, IOException {
        connectionToSQL();
        ResultSet testData = selectQuery("Select * from AmazonTable");
        ArrayList<String> listOfItems = new ArrayList<>();

        while (testData.next()) {
            String itemNames = testData.getString(1);
            listOfItems.add(itemNames);
        }

        String updateQueryStructure = "update classicmodels.AmazonTable  set searchResult= '%s' where searchableItems= '%s'";

        for (String listOfItem : listOfItems) {
            searchBox.clear();
            ExtentTestManager.log("Searchbox has been cleared");

            searchBox.sendKeys(listOfItem);
            ExtentTestManager.log("Data has been entered");

            searchButton.click();
            ExtentTestManager.log("Search box has been clicked");

            /*Assert.assertTrue(pageHeader.isDisplayed());
            String ph = pageHeader.getText();
            Assert.assertEquals(ph, listOfItems.get(i)., "Search page loading error");*/

            String rn = resultNumbers.getText();
            String finalUpdateQuery = String.format(updateQueryStructure, rn, listOfItem);
            PreparedStatement preparedStmt = connection.prepareStatement(finalUpdateQuery);
            preparedStmt.execute();
            ExtentTestManager.log("Number of results has been stored in the database");

            amazonHomePage.click();
            ExtentTestManager.log("Redirected to amazon homepage");

            searchBox.click();
            ExtentTestManager.log("Searchbox has been clicked");
        }
    }

    public void displaySuccesMessege() {
        searchBox.sendKeys("Done");
        ExtentTestManager.log("End of test");
    }
}

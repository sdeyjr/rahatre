package com.automation.pages;

import com.automation.core.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage {
    @FindBy(id = "ap_email")
    private WebElement emailField;

    @FindBy(id = "ap_password")
    private WebElement passwordField;

    @FindBy(id = "continue")
    private WebElement continueEmail;

    @FindBy(id = "signInSubmit")
    private WebElement continuePass;

    public void validateUserCanInsertEmail (String data) {
        emailField.sendKeys(data);
        continueEmail.click();
    }

    public void validateUserCanInsertPassword (String data2) {
        passwordField.sendKeys(data2);
        continuePass.click();
        TestBase.waitFor(5);
    }
}

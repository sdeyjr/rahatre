package com.automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AmazonHomePage2 {

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy (id = "nav-link-accountList")
    private WebElement signInOption;

    public void typeOnSearchBar (String data) {
        searchBox.clear();
        searchBox.sendKeys(data);
    }

    public void submitSearch () {
        searchButton.click();
    }

    public void validateUserCanClickOnSignIn () {
        signInOption.click();
    }


}

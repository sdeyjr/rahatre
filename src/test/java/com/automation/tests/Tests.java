package com.automation.tests;

import com.automation.core.TestBase;
import com.automation.pages.AmazonHomePage;
import com.automation.pages.AmazonHomePage2;
import com.automation.pages.HomePage;
import com.automation.pages.SignInPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class Tests extends TestBase {

    private HomePage homePage;
    private AmazonHomePage amazonHomePage;
    private AmazonHomePage2 amazonHomePage2;
    private SignInPage signInPage;


    @BeforeMethod
    public void instances() {
        homePage = PageFactory.initElements(driver, HomePage.class);
        amazonHomePage = PageFactory.initElements(driver, AmazonHomePage.class);
        amazonHomePage2 = PageFactory.initElements(driver, AmazonHomePage2.class);
        signInPage = PageFactory.initElements(driver, SignInPage.class);
    }

    @Test(enabled = false)
    public void validateUserCanNavigateToSignInPage() {
        homePage.validateUserCanClickSignIn();
        homePage.validateSignInPopUpLoaded();
    }

    @Test(enabled = false)
    public void validateUserCanSwitchRegions() {
        homePage.validateUserCanClickOnCountryOption();
        homePage.validateSelectCountryOptionsLoaded();
        homePage.validateUserCanClickOnCanada();
        homePage.validateUserCanSeeCanadianSite();
    }

    @Test(enabled = false)
    public void validateUserCanGoToWomensShoeSale() {
        homePage.validateUserCanGoToWomensShoeSales();
    }

    @Test(enabled = false)
    public void validateUserCanSearch() throws SQLException, IOException {
        amazonHomePage.validateUserCanClickOnSearchBox();
        amazonHomePage.validateUserCanEnterItemNamesAndSearch();
        amazonHomePage.displaySuccesMessege();
    }


    @Test(dataProvider = "dataForSearch", dataProviderClass = DataProviderClass.class, enabled = false)
    public void validateUserCanSearchForItems(String data) {
        amazonHomePage2.typeOnSearchBar(data);
        amazonHomePage2.submitSearch();
    }

    @Test(dataProvider = "dataForLogIn", dataProviderClass = DataProviderClass.class, enabled = true)
    public void validateUserCanSignIn(String username, String password) {
        amazonHomePage2.validateUserCanClickOnSignIn();
        signInPage.validateUserCanInsertEmail(username);
        signInPage.validateUserCanInsertPassword(password);
    }
}

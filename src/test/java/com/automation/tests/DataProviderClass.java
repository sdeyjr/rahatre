package com.automation.tests;

import org.testng.annotations.DataProvider;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class DataProviderClass {

    @DataProvider
    public Object[][] dataForSearch() {
        return new Object[][]{{"kitkat"}, {"hershey"}};
    }

    @DataProvider
    public Object[][] dataForLogIn() throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/logincreds.properties");
        Properties prop = new Properties();
        prop.load(fis);
        String username = prop.getProperty("username");
        String password = prop.getProperty("password");
        String username2 = prop.getProperty("username2");
        String password2 = prop.getProperty("password2");
        return new Object[][]{{username, password}, {username2, password2}};
    }
}